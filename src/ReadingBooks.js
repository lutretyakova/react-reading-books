import React, { Component } from 'react';
import './ReadingBooks.css';
import { Link } from 'react-router-dom';

import { getImageByTitle } from './image';

const REQUEST_TEMP = 'https://ru.wikipedia.org/w/api.php?origin=*&action=query&titles=200_лучших_книг_по_версии_Би-би-си&prop=revisions&rvprop=content&format=json&rvsection=1';
const BOOKS_COUNT = 4;

class ReadingBooks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            books: ''
        }
    }

    render() {
        return (
            <div className="page">
                <header className="App-header">
                    <h1>Нажми на магический шар! Сим-салабим!</h1>
                    <div onClick={() => this.handleClick()} id="button"></div>
                </header>
                {this.state.books ? this.setBooksHtml() : null}
            </div>
        );
    }


    handleClick() {
        fetch(REQUEST_TEMP)
            .then((response) => response.json()
                .then(data => {
                    this.getRandomBooks(this.getBooksList(data.query.pages));
                })
            )
            .catch((error) => console.log(error));
    }


    getBooksList(pages) {
        let pagesId = Object.keys(pages);
        let tableText = pages[pagesId].revisions[0]['*'];
        const reg = /#\s*«\[\[([^[\]]*)\]\]»\s*—\s*\[\[([^[\]]*)\|/g;
        let books = [];
        let bookInfo = '';
        do {
            bookInfo = reg.exec(tableText);
            if (bookInfo) {
                books.push([
                    bookInfo[1].split('|')[0], 
                    bookInfo[2]
                ])
            }
        } while (bookInfo !== null);
        return books
    }


    getRandomBooks(books) {
        const booksForReading = [];
        for (let i = 0; i < BOOKS_COUNT; i++) {
            let randomBook;
            do {
                randomBook = books[Math.floor(Math.random() * books.length)];
            } while (booksForReading.includes(randomBook))

            booksForReading.push(randomBook);
            
            getImageByTitle(randomBook[0]).then(
                imgUrl => this.setState({
                    [i]: imgUrl
                })
            );
        }

        this.setState({
            books: booksForReading,
        })
    }


    setBooksHtml() {
        let booksTempl = this.state.books.map((book, ind) => {
            return <div key={ind}>
                <img src={ this.state[ind] } alt="" />
                <Link to={{
                        pathname: `/book/${book[0].replace(/ /g, '_')}`,
                        state: {
                            cover: this.state[ind],
                            author: book[1]
                        }
                    }} className="book-title">{book[0]}</Link>
                <Link to={{
                        pathname: `/author/${book[1].replace(/ /g, '_')}`,
                    }}>{book[1]}</Link>
            </div>
        })
        return <div className="books">{booksTempl}</div>
    }

}

export default ReadingBooks;