import React, { Component } from "react";
import { Link } from "react-router-dom";

import { getImageByTitle } from '../image';

const BOOK_URL_TMP = 'https://ru.wikipedia.org/w/api.php?origin=*&action=query&prop=revisions&rvprop=content&format=json&titles=';
const DEFINITION_WORD_LENGTH = 60;

class BookPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            info: '',
            author: this.props.location.state ? this.props.location.state.author : '',
            title: this.props.match.params.title.replace(/_/g, ' ') || '',
            urlTitle: this.props.match.params.title,
            cover: '',
            cycleName: '',
            previousInCycle: '',
            nextInCycle: '',
            aboutBook: '',
            fate: 'Звезды вам к вам не благосколонны. Посмотрите кино!',
            isError: false,
        }
    }

    render() {
        const content = this.state.isError
                        ? this.getErrorContent()
                        : this.getPageContent()
        return (
            content 
        )
    }


    getErrorContent(){
        return (
            <div className="page">
               <header>
                   <h1>{this.state.fate}</h1>
                </header>
            </div>
        )
    }


    getPageContent() {
        const cover = <img src= { this.state.cover }
                            alt={this.state.title} className="cover" />;
        const cycleName = this.state.cycleName 
                            ? <div><b>цикл:</b> {this.state.cycleName}</div> 
                            : null;
        const previous = this.state.previousInCycle 
                            ? <div><b>предыдущая:</b> {this.state.previousInCycle}</div> 
                            : null;
        const next = this.state.nextInCycle 
                            ? <div><b>следующая:</b> {this.state.nextInCycle}</div> 
                            : null;
        return( 
            <div className="page">
                { cover }
                <h1 className="title">"{this.state.title}"</h1>
                <h2 className="author">
                    <Link to={{pathname: `/author/${this.state.author.replace(/ /g, '_')}`}}>{this.state.author}</Link>
                </h2>
                {cycleName}
                {previous}
                {next}
                <div className="about">
                    {this.state.aboutBook}
                </div>
            </div>
        )
    }
    

    componentDidMount() {
        const bookUrl = BOOK_URL_TMP.concat(this.props.match.params.title);
        this.getDataFromUrl(bookUrl).then(() => {
            getImageByTitle(this.state.urlTitle).then(
                imgUrl => this.setState({
                    cover: imgUrl
                })
            )
        })
    }


    getDataFromUrl(bookUrl){
        return fetch(bookUrl)
            .then((response) => response.json()
                .then(data => {
                    
                    let pagesId = Object.keys(data.query.pages).filter(ids => ids > 0)[0];
                    if (!pagesId) {
                        this.setState({ isError: true })
                        return
                    }
                    let info = data.query.pages[pagesId].revisions[0]['*'];
                    const redirectReg = /#(?:перенаправление|ПЕРЕНАПРАВЛЕНИЕ|REDIRECT|redirect)\s+\[\[(.*)\]\]/g; 
                    if(info.search(redirectReg) !== -1){
                        let newTitle = redirectReg.exec(info)[1].replace(/\s/g, '_');
                        this.setState({
                            urlTitle: newTitle
                        });
                        let newUrl = BOOK_URL_TMP.concat(newTitle);
                        this.getDataFromUrl(newUrl);
                        return
                    }
                    
                    this.getBookInfo(info);
                })
            )
            .catch((error) => this.setState({ isError: true }))

    }


    getBookInfo(bookInfo) {
        const titleReg = /(?:Название|РусНаз)\s*=\s*(.*)\n/g;
        let title = titleReg.exec(bookInfo);
        title = title 
                    ? title[1].replace(/^\s+|\s+$/g, '') 
                    : this.state.title;
        
        const authorReg = /Автор\s*=\s*(.*)\n/g;
        let author = authorReg.exec(bookInfo);
        author = author
                    ? author[1]
                        .replace(/^\s+|\s+$/g, '')
                        .replace(/\[\[|\]\]/g, '')
                    : '';
        author = author 
                    ? author.split('|')[1] || author.split('|')[0] 
                    : this.state.author;

        const cycleReg = /(?:Серия|Цикл)\s*=\s*(.*)\n/g;
        let cycleName = cycleReg.exec(bookInfo);
        cycleName = cycleName 
                        ? cycleName[1]
                            .replace(/^\s+|\s+$/g, '')
                            .replace(/\[\[|\]\]/g, '')
                            .split('|')[0] 
                        : '';

        const nextReg = /Следующая\s*=\s*(.*)\n/g;
        let nextInCycle = nextReg.exec(bookInfo);
        nextInCycle = nextInCycle 
                        ? nextInCycle[1]
                            .replace(/^0/g, '')
                            .replace(/^\s+|\s+$/g, '')
                            .replace(/\[\[|\]\]/g, '')
                            .split('|')[0] 
                        : '';

        const previousReg = /Предыдущая\s*=\s*(.*)\n/g;
        let previousInCycle = previousReg.exec(bookInfo);
        previousInCycle = previousInCycle 
                            ? previousInCycle[1]
                                .replace(/^0/g, '')
                                .replace(/^\s+|\s+$/g, '')
                                .replace(/\[\[|\]\]/g, '')
                                .split('|')[0] 
                            : '';

        const aboutBook = this.getBookDefinition(bookInfo);
        
        if (author && title && (aboutBook || cycleName || previousInCycle || nextInCycle)) {
            this.setState({
                isError: false,
                title,
                author,
                cycleName,
                previousInCycle,
                nextInCycle,
                aboutBook
            })
        } else {
            this.setState({
                isError: true
            })
        }
    }


    getBookDefinition(bookInfo) {
        const aboutTextValues = [
            "Часть", 
            "Сюжет", 
            "Сюжет и темы",
            "Содержание", 
            "Описание сюжета", 
            "Аннотация"];

        let definition = '';
        for (let aboutText of aboutTextValues) {
            let reg = new RegExp(aboutText.concat('\\s+=='), 'g');
            let index = bookInfo.search(reg);
            if (index !== -1) {
                definition = bookInfo.substring(index);
                break;
            }
        }
        if (definition === '') {
            return
        }

        // 0-элемент: == после слова Сюжет
        // 1-элемент: описание книги
        // 2-элемент: == перед началом следующего блока
        definition = definition.split(' ==\n')[1];

        // если после слова Сюжет есть описание частей, например, ===Часть первая===,
        // то ведущее = нужно убрать
        console.log(definition)
        definition = definition.replace(/^=*/g, '');
        
        // удалить внутренние ссылки и разметку
        definition = definition.replace(/\[\[^|]*\|/g, '')
                                .replace(/\*\s+'''/g, '')
                                .replace(/'''/g, '')
                                .replace(/\[\[Файл:.*\]\]/g, '')
                                .replace(/\]\]|\[\[/g, '')
                                .replace(/\(\{\{.*\}\}\)/g, '')
                                .replace(/\{\{.*\}\}/g, '')
                                .replace(/(<ref.*\/ref>)/g, '')
                                .replace(/^[\s.,/#!$%^&*;:{}=\-_`~()]*/g, '')
                                .split(' ').slice(0, DEFINITION_WORD_LENGTH).join(' ')
                                .replace(/[\s.,/#!$%^&*;:{}=\-_`~()—]*$/g, '')
                                .concat('...')
        return definition
    }
}

export default BookPage;