import React, { Component } from 'react';
import { getImageByTitle } from '../image';

const AUTHOR_URL_TMP = 'https://ru.wikipedia.org/w/api.php?origin=*&action=query&prop=revisions&rvprop=content&format=json&titles=';

class AuthorPage extends Component {
    render() {
        const content = this.state.isError 
                            ? this.getErrorContent()
                            : this.getPageContent()
        return content
    }


    constructor(props) {
        super(props);
        this.state = {
            isError: false,
            fate: 'Звёзды скрылись за облаками. Может, сериал?',
            urlTitle: this.props.match.params.authorName,
            name: '',
            about: '',
            photo: '',
            catalog: []
        }
    }


    getErrorContent(){
        return (
            <div className="page">
               <header>
                   <h1>{this.state.fate}</h1>
                </header>
            </div>
        )
    }


    getPageContent(){
        const photo = <img src= { this.state.photo }
                            alt={this.state.name} className="cover" />;

        const catalog = this.state.catalog.length !== 0
                            ?
                            <div className="catalog">
                                <h2>Библиография</h2>
                                <ul>
                                    { this.state.catalog.map((item, ind) =>
                                        <li key={`cat-${ind}`}>{item}</li>)
                                    }
                                </ul>
                            </div>
                            : null
        return (
            <div className="page">
                { photo }
                <h1 className="title">
                    { this.state.name }
                </h1>
                <div className="about">
                    { this.state.about }
                </div>
                { catalog }
            </div>
        )
    }

    componentDidMount() {
        const authorUrl = AUTHOR_URL_TMP.concat(this.props.match.params.authorName);
        
        this.getDataFromUrl(authorUrl).then(() =>
            this.setState({
                photo: getImageByTitle(this.state.urlTitle).then(
                    imgUrl => this.setState({
                        photo: imgUrl
                    })
                )
            })
        )
    }


    getDataFromUrl(url) {
        return fetch(url)
            .then(response => response.json())
                .then(data => {
                    const pageId = Object.keys(data.query.pages).filter(ids => ids > 0)[0];
                    if (!pageId) {
                        this.setState({ isError: true })
                        return
                    }

                    const info = data.query.pages[pageId].revisions[0]['*'];
                    const redirectReg = /#(?:перенаправление|ПЕРЕНАПРАВЛЕНИЕ|REDIRECT|redirect)\s*\[\[(.*)\]\]/g; 
                    if(info.search(redirectReg) !== -1){
                        let newTitle = redirectReg.exec(info)[1].replace(/\s/g, '_');
                        this.setState({
                            urlTitle: newTitle
                        });
                        let newUrl = AUTHOR_URL_TMP.concat(newTitle);
                        this.getDataFromUrl(newUrl);
                        return
                    }
                    this.getAuthorData(info);
            })
            .catch(error => this.setState({ isError: true }))

    }


    getAuthorData(info) {
        const nameReg = /Имя\s*=\s*(.*)\n/g;
        let name = nameReg.exec(info)
        name = name 
                ? name[1]
                : this.props.match.params.authorName.replace(/_/g, ' ')

        let about = '';
        const startAboutSectionReg = /(?:\n[^\s]*\s*)?'''/g;
        if (info.search(startAboutSectionReg) !== -1){
            const subParagraphIndex = info.search(/#[A-Za-zА-Яа-яЁё\s]*\|/);
            const splitChar = subParagraphIndex !== -1 && subParagraphIndex < info.indexOf('==')
                                ? '#'
                                : '==';
            about = info.substring(info.search(startAboutSectionReg)).split(splitChar)[0]
                        .replace(/(<!--.*-->)/g, '')
                        .replace(/(<[^>]*>)/g, '')
                        .replace(/{{нп[0-9]\|([^|]*)\|(?:[^}]*)}}/g,'$1')
                        .replace(/\{\{[^|]*\|([0-9]*\|[0-9А-Яа-я]*\|[0-9]*)(?:\|[^}]*)?\}\}/g, '$1')
                        .replace(/\|К\|[^|]*\|К/g, '')
                        .replace(/\{\{[^}]*}}/g, '')
                        .replace(/{{[^|]*\|/g, '')
                        .replace(/\[\[([^|[\]]*)\|/g, '')
                        .replace(/\[\[|\]\]/g, '')
                        .replace(/\[http[^\]]*\]/g, '')
                        .replace(/'''/g, '')
                        .replace(/(''}})|}}/g, '')
                        .replace(/\([;,.\s]*/g, '(')
                        .replace(/\s*\(\)\s*/g, '')
                        .replace(/\s*;\s*;/g, ';')
        }

        let booksInd = info.search(/==\s*(Книги|Библиография|Публикации|Произведения|Основные\s*произведения|Романы,\s*сборники\s*рассказов|Работы|Романы\s*и\s*новеллы)\s*==/g);
        
        if (booksInd !== -1){
            let booksInfo = info.substring(booksInd).split(" ==\n")[1];
            let booksReg = /(?:\*|#)\s*([^\n]*)/g//   (?:(?:«\[\[)|(?:\{\{нп2\|))?([^\n]*)/g; 
            let catalog = [];
            let book;
            do {
                book = booksReg.exec(booksInfo);
                if (book){
                    let bookTitle = book[1]
                                    .replace(/\[\[(?:[^|\]]*\|)?([^\]]*)\]\]/g, '$1')
                                    .replace(/\s*{{(?:(?:нп[0-9])|(?:iw))\|([^|}]*)(?:[^}]*)\}\}/g, ' $1')
                                    .replace(/тираж.*/g, '')
                                    .replace(/\[http.*/g , '')
                                    .replace(/\[\[|\]\]/g, '')
                                    .replace(/\*/, '')
                                    .replace(/<[^>]*>/g, '')
                                    .replace(/\(<[^)]*\)/g, '')
                                    .replace(/''/g, '')
                                    .replace(/\{\{.*$/g, '')
                                    .replace(/[.,«(;]{1}\s*$/g, '')
                                    .replace(/^\s*$/g, '')
                    if  (bookTitle && !catalog.includes(bookTitle)){
                        catalog.push(bookTitle)
                    }
                }
            } while (book !== null);

            this.setState({
                catalog
            })
        }

        this.setState({
            name,
            about
        })
    }
}

export default AuthorPage;