import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { createStore } from 'redux';
import { HashRouter, Route } from 'react-router-dom';

import registerServiceWorker from './registerServiceWorker';

import './index.css';
import ReadingBooks from './ReadingBooks';
import BookPage from './modules/BookPage';
import AuthorPage from './modules/AuthorPage';

const reducer = (state = {}, action) => {
    switch (action.type) {
        default:
            return state
    }
}

const store = createStore(reducer);

ReactDOM.render(
    <Provider store={ store }>
        <HashRouter>
            <div>
                <Route exact path="/" component={ReadingBooks} />
                <Route path="/book/:title" component={BookPage}/>
                <Route path="/author/:authorName" component={AuthorPage} />
            </div>
        </HashRouter>
    </Provider>, 
    document.getElementById('root')
);
registerServiceWorker();
