import defaultCover from './images/default_cover.svg';

const IMAGES_LINK_TEMP = 'https://ru.wikipedia.org/w/api.php?origin=*&action=query&prop=pageimages&format=json&piprop=original&titles='

export const getImageByTitle = (book) => {
    return fetch(IMAGES_LINK_TEMP.concat(book.replace(/ /g, '_', "g")))
        .then((response) => response.json()
            .then(data => {
                let id = Object.keys(data.query.pages)[0];
                let cover = 'original' in data.query.pages[id] 
                                ? data.query.pages[id].original.source 
                                : defaultCover;
                return cover
            })
        )
        .catch((error) => console.log(error))
}