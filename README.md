# Книжный предсказатель
Книжный предсказатель предлагает вам на выбор несколько книг для прочтения. 
О каждой книге и об авторе можно получить дополнительную информацию.

## Demo
[Может, немного погадаем?](https://lutretyakova.gitlab.io/projects/react-book-forecaster/)

## Блог
[Блог](https://lutretyakova.gitlab.io/react-book-forecaster.html)